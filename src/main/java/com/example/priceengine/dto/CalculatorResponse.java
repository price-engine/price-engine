package com.example.priceengine.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Thanura Rashmika
 */
@Data
public class CalculatorResponse {
    private String productCode;
    private String productName;
    private Integer noOfCartons;
    private BigDecimal cartonPrice;
    private Integer manualPacking;
    private BigDecimal individualPrice;
    private BigDecimal discount = BigDecimal.ZERO;
    private BigDecimal packingCharges = BigDecimal.ZERO;
    private BigDecimal total = BigDecimal.ZERO;
}
