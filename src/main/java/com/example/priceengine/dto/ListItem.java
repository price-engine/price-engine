package com.example.priceengine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Thanura Rashmika
 */
@Data
@AllArgsConstructor
public class ListItem {
    private String value;
    private String label;
}
