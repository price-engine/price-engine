package com.example.priceengine.controller;

import com.example.priceengine.dto.CalculatorResponse;
import com.example.priceengine.exception.ProductNotFoundException;
import com.example.priceengine.service.PriceEngineService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Thanura Rashmika
 */
@RestController
@RequestMapping("/api/v1/calculate")
@AllArgsConstructor
@Slf4j
public class CalculatorController {

    private final PriceEngineService priceEngineService;

    @CrossOrigin
    @GetMapping("/")
    public ResponseEntity<CalculatorResponse> calculateTotal(@RequestParam String productCode, @RequestParam Integer count) {
        try {
            return ResponseEntity.ok().body(priceEngineService.calculateTotal(productCode, count));
        } catch (ProductNotFoundException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CalculatorResponse());
        }
    }
}
