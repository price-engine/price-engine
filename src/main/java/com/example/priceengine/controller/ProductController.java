package com.example.priceengine.controller;

import com.example.priceengine.dto.ListItem;
import com.example.priceengine.service.PriceEngineService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Thanura Rashmika
 */
@RestController
@RequestMapping("/api/v1/product")
@AllArgsConstructor
public class ProductController {

    private final PriceEngineService priceEngineService;

    @CrossOrigin
    @GetMapping("/")
    public ResponseEntity<List<ListItem>> getProductsList() {
        return ResponseEntity.ok().body(priceEngineService.getProductList());
    }
}
