package com.example.priceengine.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Thanura Rashmika
 */
@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;
    @Column(unique = true)
    private String productCode;
    private String name;
    private String description;
    private BigDecimal cartonPrice;
    private Integer unitsPerCarton;
}
