package com.example.priceengine.exception;

/**
 * @author Thanura Rashmika
 */
public class ProductNotFoundException extends Exception {
    private static final String message = "Product Not Found";

    public ProductNotFoundException() {
        super(message);
    }
}
