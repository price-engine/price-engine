package com.example.priceengine.repository;

import com.example.priceengine.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * @author Thanura Rashmika
 */
public interface ProductsRepository extends CrudRepository<Product, Long> {
    Optional<Product> findByProductCode(String productCode);
}
