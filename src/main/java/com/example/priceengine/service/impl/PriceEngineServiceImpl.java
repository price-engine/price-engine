package com.example.priceengine.service.impl;

import com.example.priceengine.dto.CalculatorResponse;
import com.example.priceengine.dto.ListItem;
import com.example.priceengine.entity.Product;
import com.example.priceengine.exception.ProductNotFoundException;
import com.example.priceengine.repository.ProductsRepository;
import com.example.priceengine.service.PriceEngineService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Thanura Rashmika
 */
@Service
@AllArgsConstructor
public class PriceEngineServiceImpl implements PriceEngineService {

    private final ProductsRepository productsRepository;

    @Override
    public CalculatorResponse calculateTotal(String productCode, Integer count) throws ProductNotFoundException {
        Product product = productsRepository.findByProductCode(productCode)
                .orElseThrow(ProductNotFoundException::new);

        int packedManually = count % product.getUnitsPerCarton();
        int noOfCartons = count / product.getUnitsPerCarton();

        CalculatorResponse calculatorResponse = new CalculatorResponse();
        calculatorResponse.setProductCode(product.getProductCode());
        calculatorResponse.setProductName(product.getName());
        calculatorResponse.setCartonPrice(product.getCartonPrice());
        calculatorResponse.setManualPacking(packedManually);
        calculatorResponse.setNoOfCartons(noOfCartons);
        calculatorResponse.setIndividualPrice(product.getCartonPrice()
                .divide(BigDecimal.valueOf(product.getUnitsPerCarton()), 2, RoundingMode.HALF_UP));

        if (packedManually > 0 && noOfCartons > 0) {
            calculateManualPackaging(calculatorResponse, packedManually, product);
            calculateCarton(calculatorResponse, noOfCartons, product);
        } else if (packedManually > 0) {
            calculateManualPackaging(calculatorResponse, packedManually, product);
        } else {
            calculateCarton(calculatorResponse, noOfCartons, product);
        }

        return calculatorResponse;
    }

    void calculateManualPackaging(CalculatorResponse calculatorResponse, int packedManually, Product product) {
        BigDecimal packagingCost =
                product.getCartonPrice().multiply(BigDecimal.valueOf(0.3)).setScale(2, RoundingMode.HALF_UP);
        calculatorResponse.setTotal(
                BigDecimal.valueOf(packedManually).multiply(calculatorResponse.getIndividualPrice()).add(packagingCost)
        );
        calculatorResponse.setPackingCharges(packagingCost);
    }

    void calculateCarton(CalculatorResponse calculatorResponse, int noOfCartons, Product product) {
        int discountRange = 3;
        BigDecimal discount = BigDecimal.ZERO;
        if (noOfCartons >= discountRange) {
            discount = product.getCartonPrice()
                    .multiply(BigDecimal.valueOf(0.1)).setScale(2, RoundingMode.HALF_UP);
        }
        calculatorResponse.setTotal(
                calculatorResponse.getTotal()
                        .add(BigDecimal.valueOf(noOfCartons).multiply(product.getCartonPrice()))
                        .subtract(discount)
                        .setScale(2, RoundingMode.HALF_UP)
        );
        calculatorResponse.setDiscount(discount);
    }

    @Override
    public void saveInitialProducts(List<Product> products) {
        products.forEach(product -> {
            if (productsRepository.findByProductCode(product.getProductCode()).isEmpty()) {
                productsRepository.save(product);
            }
        });
    }

    @Override
    public List<ListItem> getProductList() {
        List<ListItem> productList = new ArrayList<>();
        productsRepository.findAll().forEach(product -> productList.add(
                new ListItem(product.getProductCode(), product.getName())
        ));
        return productList;
    }
}
