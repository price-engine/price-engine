package com.example.priceengine.service;

import com.example.priceengine.dto.CalculatorResponse;
import com.example.priceengine.dto.ListItem;
import com.example.priceengine.entity.Product;
import com.example.priceengine.exception.ProductNotFoundException;

import java.util.List;

/**
 * @author Thanura Rashmika
 */
public interface PriceEngineService {
    CalculatorResponse calculateTotal(String productCode, Integer count) throws ProductNotFoundException;

    void saveInitialProducts(List<Product> products);

    List<ListItem> getProductList();
}
