package com.example.priceengine;

import com.example.priceengine.dto.CalculatorResponse;
import com.example.priceengine.dto.ListItem;
import com.example.priceengine.entity.Product;
import com.example.priceengine.exception.ProductNotFoundException;
import com.example.priceengine.service.PriceEngineService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class PriceEngineApplicationTests {

    @Autowired
    private PriceEngineService priceEngineService;

    private final String penguinEarsCode = "RP001";
    private final String horseShoeCode = "RGP001";

    @Test
    void contextLoads() {
    }

    @BeforeEach
    void initialize() {
        List<Product> products = new ArrayList<>();
        Product penguinEars = new Product();
        Product horseShoe = new Product();

        penguinEars.setProductCode(penguinEarsCode);
        penguinEars.setName("Penguin Ears");
        penguinEars.setDescription("Rare Product");
        penguinEars.setCartonPrice(BigDecimal.valueOf(175));
        penguinEars.setUnitsPerCarton(20);

        horseShoe.setProductCode(horseShoeCode);
        horseShoe.setName("Horse Shoe");
        horseShoe.setDescription("Regular Product");
        horseShoe.setCartonPrice(BigDecimal.valueOf(825));
        horseShoe.setUnitsPerCarton(5);

        products.add(penguinEars);
        products.add(horseShoe);
        priceEngineService.saveInitialProducts(products);
    }

    @Test
    void calculatePenguinEarsCarton() throws ProductNotFoundException {
        BigDecimal expected = BigDecimal.valueOf(175.00).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(penguinEarsCode, 20);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(1, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(175.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(0, calculatorResponse.getManualPacking()),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getPackingCharges()),
                () -> assertEquals(
                        BigDecimal.valueOf(8.75).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getDiscount()),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculatePenguinEarsSingle() throws ProductNotFoundException {
        // expected = (175/20)+(175*0.3) = 61.25
        BigDecimal expected = BigDecimal.valueOf(61.25).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(penguinEarsCode, 1);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(0, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(175.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(1, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.valueOf(52.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(8.75).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getDiscount()),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculate25PenguinEars() throws ProductNotFoundException {
        // expected = 175+(175/20)*5+(175*0.3) = 271.25
        BigDecimal expected = BigDecimal.valueOf(271.25).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(penguinEarsCode, 25);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(1, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(175.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(5, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.valueOf(52.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(8.75).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getDiscount()),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculate60PenguinEars() throws ProductNotFoundException {
        // expected = 175*3-(175*0.1) = 507.50
        BigDecimal expected = BigDecimal.valueOf(507.50).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(penguinEarsCode, 60);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(3, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(175.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(0, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.ZERO,
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(8.75).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(17.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getDiscount()
                ),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculate79PenguinEars() throws ProductNotFoundException {
        // expected = (175*3)+(175/20)*19+(175*0.3)-(175*0.1) = 726.25
        BigDecimal expected = BigDecimal.valueOf(726.25).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(penguinEarsCode, 79);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(3, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(175.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(19, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.valueOf(52.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(8.75).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(17.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getDiscount()
                ),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculateHorseShoeCarton() throws ProductNotFoundException {
        BigDecimal expected = BigDecimal.valueOf(825.00).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(horseShoeCode, 5);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(1, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(825.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(0, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.ZERO,
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(165.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getDiscount()),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculateHorseShoeSingle() throws ProductNotFoundException {
        //expected = (825/5)+(825*0.3) = 412.50
        BigDecimal expected = BigDecimal.valueOf(412.50).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(horseShoeCode, 1);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(0, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(825.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(1, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.valueOf(247.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(165.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getDiscount()),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculate9HorseShoes() throws ProductNotFoundException {
        //expected = 825+(825/5)*4+(825*0.3) = 1732.50
        BigDecimal expected = BigDecimal.valueOf(1732.50).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(horseShoeCode, 9);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(1, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(825.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(4, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.valueOf(247.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(165.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(BigDecimal.ZERO, calculatorResponse.getDiscount()),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculate15HorseShoes() throws ProductNotFoundException {
        //expected = 825*3-(825*0.1) = 2392.50
        BigDecimal expected = BigDecimal.valueOf(2392.50).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(horseShoeCode, 15);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(3, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(825.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(0, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.ZERO,
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(165.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(82.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getDiscount()
                ),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void calculate24HorseShoes() throws ProductNotFoundException {
        //expected = (825*4)+(825/5)*4+(825*0.3)-(825*0.1) = 4125
        BigDecimal expected = BigDecimal.valueOf(4125.00).setScale(2, RoundingMode.HALF_UP);
        CalculatorResponse calculatorResponse = priceEngineService.calculateTotal(horseShoeCode, 24);
        Assertions.assertAll(
                () -> assertNotNull(calculatorResponse),
                () -> assertEquals(4, calculatorResponse.getNoOfCartons()),
                () -> assertEquals(
                        BigDecimal.valueOf(825.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getCartonPrice()
                ),
                () -> assertEquals(4, calculatorResponse.getManualPacking()),
                () -> assertEquals(
                        BigDecimal.valueOf(247.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getPackingCharges()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(165.00).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getIndividualPrice()
                ),
                () -> assertEquals(
                        BigDecimal.valueOf(82.50).setScale(2, RoundingMode.HALF_UP),
                        calculatorResponse.getDiscount()
                ),
                () -> assertEquals(expected, calculatorResponse.getTotal())
        );
    }

    @Test
    void getAllProducts() {
        List<ListItem> productList = priceEngineService.getProductList();
        Assertions.assertAll(
                () -> assertNotNull(productList),
                () -> assertEquals(2, productList.size())
        );
    }
}
